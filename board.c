// board.c
// 14 Feb 2007

#include <assert.h>

int getStone(const Position * const pos, const int where){
    return pos->stones[where];
}

void setStone(Position * const pos, const int where,
              const int newValue, const int oldValue){
    pos->counts[oldValue]--;
    pos->counts[newValue]++;

    pos->hash ^= HashContribs[where][oldValue ^ newValue];

    const int quo = where / BFWIDTH,
              mod = where % BFWIDTH;

    if (newValue){
        pos->empty[quo] &= ~(1 << mod);
    } else {
        pos->empty[quo] |=  (1 << mod);
    }

    pos->stones[where] = newValue;
}

void emptyPosition(Position * pos){
    memset(pos, 0, sizeof(*pos));
    pos->turn = 1;
    memset(pos->empty, 255, sizeof(pos->empty));
    setStone(pos, 0, 1, 0);
    pos->counts[1] = 0;
    pos->lastMoves[0] = pos->lastMoves[1] = -1;
}

void copyPosition(Position * dst, Position * src){
    *dst = *src;
}

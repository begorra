// shell.c
// Copyright 2008 Tom Plick.
// Begorra is released under GPL v2: see file COPYING for info.

#include <string.h>
#include <ctype.h>

char columnLetter(int col){
    char x = 'A' + col;
    if (x >= 'I') x++;
    return x;
}

char POINT_NAMES[10 * BOARD_SIZE];
void initPointNames(){
    POINT_NAMES[0] = 0;
    char one[5];
    char * s = POINT_NAMES;

    s = strcat(s, " PASS ");
    for (int r = 1; r <= EDGE; r++){
        for (int c = 0; c < EDGE; c++){
            sprintf(one, " %c%-2d  ", columnLetter(EDGE - 1 - c), r);
            s = strcat(s, one);
        }
    }
}

int pointFromName(char * name){
    char aname[7];
    strcpy(aname, " ");
    strncat(aname, name, 5);
    strcat(aname, " ");

    for (char * ch = aname; *ch; ch++)  *ch = toupper(*ch);

    char * found = strstr(POINT_NAMES, aname);
    if (found)
        return (found - POINT_NAMES) / 6;
    else
        return -1;
}


char symbols[4] = "_BW";
char titles[3][10] = {"no one", "Black", "White"};
char * drawPosition(Position * pos){
    static char string[STRING_REP_SIZE], part[] = "  ", *s;
    char scratch[10];
    s = string; s[0] = 0;
    int row = EDGE;
    OVER_BOARD(i){
        if (i && i % EDGE == 0){
            s = strcat(s, "\n");
            sprintf(scratch, "%2d  ", row--);
            s = strcat(s, scratch);
        }
        part[0] = symbols[getStone(pos, i)];
        s = strcat(s, part);
    }
    s = strcat(s, "\n    ");
    for (int i = 0; i < EDGE; i++){
        sprintf(scratch, "%c ", columnLetter(i));
        s = strcat(s, scratch);
    }
    s = strcat(s, "\n      ");
    s = strcat(s, titles[pos->turn]);
    s = strcat(s, " to move");
    return string;
}

#include <errno.h>
#include <sys/types.h>
#include <sys/times.h>
#include <sys/select.h>

// for BeOS
#ifdef __BEOS__
#include <net/socket.h>
#endif

static fd_set set;
static struct timeval timeout = {0, 0};

int isLineWaiting(){
    FD_ZERO(&set); FD_SET(0, &set);
    return (select(FD_SETSIZE, &set, NULL, NULL, &timeout) > 0);
}

void showNumber(abInfo * info, int success){
    if (success) printf("="); else printf("?");

    if (info->commandNumber >= 0)
        printf("%d", info->commandNumber);
    printf(" ");
}
#define header showNumber

void quit(abInfo * info){
    header(info, 1);
    printf("Execution complete.  See you on the flip side.\n");
    exit(0);
}

void setBoardSize(abInfo * info){
    header(info, 0);
    printf("Board size is set to %d by %d and cannot be changed.\n",
          EDGE, EDGE);
}

void showHelp(abInfo *);


void showParams(abInfo * info){
    header(info, 1);
    printf("Compile-time parameters:\n");
    printf("    Board size: %d by %d\n", EDGE, EDGE);
    printf("    Size of recommendation table: %d MB, %d million slots\n",
                    HASHMEM, HASHSIZE / 1000000);
}

void showName(abInfo * info){
    header(info, 1);
    printf("Begorra\n");
}

void showVersion(abInfo * info){
    header(info, 1);
    printf("0.0.0\n");
}

void listCommands(abInfo *);
void knownCommand(abInfo *);

void displayBoard(abInfo * info){
    header(info, 1);
    char * s = drawPosition(&info->pos);
    printf("%s\n\n", s);
}

void clearBoard(abInfo * info){
    info->aborted = 1;
    emptyPosition(&info->pos);
    displayBoard(info);
}

void makeMove(abInfo * info){
    char * point = strtok(NULL, " \n");
    if (!point){
        header(info, 0);
        printf("Must give point or `pass'\n");
        return;
    }
    int pt = pointFromName(point);
    if (pt < 0){
        header(info, 0);
        printf("Invalid point.\n");
        return;
    }

    if (getStone(&info->pos, pt)){
        header(info, 0);
        printf("Point is not empty.\n");
        return;
    }

    if (pt)
        placeStone(&info->pos, pt);
    else
        passTurn(&info->pos);
    info->aborted = 1;
    displayBoard(info);
}

struct {
    char command[20];
    void (*handler)(abInfo *);
    char desc[100];
} Handlers[20] = {
    {"boardsize", setBoardSize, "(not implemented)"},
    {"clear_board", clearBoard, "Reset board to start of game"},
    {"display", displayBoard, "Print out the current state of the game"},
    {"known_command", knownCommand},
    {"list_commands", listCommands, "List all supported commands"},
    {"name", showName, "Print out your humble engine's name"},
    {"play", makeMove},
    {"quit", quit, "Exactly what you think"},
    {"version", showVersion, "print engine version"},
    {"?", showHelp, "Show this help screen"},
    {"!", showParams, "Show parameters set at compile-time"},
    {"", NULL}
};

#define MATCH(a, b)   else if (strcmp(a, b) == 0)

int isDigit(char c){
    return c >= '0' && c <= '9';
}

void handleCommand(char * line, abInfo * info)
{
    char * command = strtok(line, " \n");
    if (!command) return;

    info->commandNumber = -1;
    if (isDigit(command[0])){
        info->commandNumber = atoi(command);
        command = strtok(NULL, " \n");
        if (!command) return;
    }

    for (int i = 0; Handlers[i].handler; i++){
        if (strstr(Handlers[i].command, command) == Handlers[i].command){
            Handlers[i].handler(info);
            return;
        }
    }

    printf("%s: Unrecognized command.  Type `?' to see command list.\n", command);
}


static char LINE[81];
void pollForInput(abInfo * info){
    while (isLineWaiting()){
        fgets(LINE, sizeof(LINE), stdin);
        handleCommand(LINE, info);
    }
}

void listCommands(abInfo * info){
    showNumber(info, 1);
    for (int i = 0; Handlers[i].handler; i++)
        printf("%s\n", Handlers[i].command);
    printf("\n\n");
}

void knownCommand(abInfo * info){
    showNumber(info, 1);
    char * command = strtok(NULL, " \n");
    if (!command){
        printf("false\n");
        return;
    }
    for (int i = 0; Handlers[i].handler; i++){
        if (strcmp(Handlers[i].command, command) == 0){
            printf("true\n");
            return;
        }
    }
    printf("false\n");
}

void showHelp(abInfo * info){
    if (info)
        header(info, 1);
    printf("Begorra v0.0.  Commands:\n");
    for (int i = 0; Handlers[i].handler; i++){
        printf("  %-15s %s\n", Handlers[i].command, Handlers[i].desc);
    }
    printf("Commands may be abbreviated (e.g. `d' for `display').\n\n");
}

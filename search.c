// search.c
// Copyright 2008 Tom Plick.
// Begorra is released under GPL v2: see file COPYING for info.

// Alpha-beta and related functions.

#include "types.c"

double getPreciseTime(){
    struct timeval tv;
    gettimeofday(&tv, NULL);
    return tv.tv_sec + tv.tv_usec / 1000000.0;
}

abInfo newABInfo(){
    abInfo inf;
    inf.nodes = 0ULL;
    inf.aborted = 0;
    inf.startTime = getPreciseTime();
    return inf;
}


#define INSERT_INTO_TABLE(vEntry, vDepth, vPoint) \
    do { \
        if (!info->aborted && vDepth > vEntry->depth){ \
            vEntry->depth = vDepth; \
            vEntry->point = vPoint; \
        } \
    } while (0)

// XXX Remove the "best" fuss; just get the best afterward from the
//      table.  This will force the table to be at least 1K...

int alphaBeta(Position * const restrict pos, const int depth,
              int * const restrict best,
              int alpha, const int beta, abInfo * const info)
{
    if (info->aborted) return 0;

    if (++info->nodes % (1 << POLL_FREQUENCY) == 0){
        pollForInput(info);
    }

    if (depth == 0)
        return tallyPos(pos);

    Position child;
    int q = 0;

    unsigned int slot = pos->hash % HASHSIZE;
    HashEntry * entry = &TransTable[slot];
    const int firstPoint = entry->point;

#define ALPHA_BETA_STEP  \
    do { \
        copyPosition(&child, pos);  \
        placeStone(&child, pt); \
        int value = -alphaBeta(&child, depth - 1, &q, -beta, -alpha, info); \
        if (value >= beta){ \
            INSERT_INTO_TABLE(entry, depth, pt); \
            return beta; \
        } else if (value > alpha){ \
            alpha = value; \
            *best = pt; \
        } \
    } while(0);

    do {
        const int pt = firstPoint;
        if (pt){
            if (pt == pos->lastMoves[0]) continue;
            if (getStone(pos, pt)) continue;

            ALPHA_BETA_STEP;
        }
    } while (0);

    int adjustment = 0;
    for (int i = 0; i < BFLENGTH; i++){
        BoardInt field = pos->empty[i];
        if (i == BFLENGTH - 1)
            field &= ((1 << (SIZE % BFWIDTH)) - 1);
        if (i == firstPoint / BFWIDTH)
            field  &=  ~(1 << (firstPoint % BFWIDTH));
        if (i == pos->lastMoves[0] / BFWIDTH)
            field  &=  ~(1 << (pos->lastMoves[0] % BFWIDTH));

        while (field){
            int bit = getLowestBit(field);
            int pt = bit + adjustment;

            ALPHA_BETA_STEP;
            field -= (1 << bit);
        }
        adjustment += BFWIDTH;
    }

    // a passing move

    passTurn(pos);
    int value = -alphaBeta(pos, depth - 1, &q, -beta, -alpha, info);
    passTurn(pos);

    if (value >= beta){
        INSERT_INTO_TABLE(entry, depth, 0);
        return beta;
    }
    if (value > alpha){
        alpha = value;
        *best = 0;
    }

    INSERT_INTO_TABLE(entry, depth, *best);
    return alpha;
}

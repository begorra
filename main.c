// main.c
// Copyright 2008 Tom Plick.
// Begorra is released under GPL v2: see file COPYING for info.

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <unistd.h>
#include <sys/time.h>


// EDGE must be defined when this file is compiled,
//    e.g. gcc -DEDGE=5 board.c
#ifndef EDGE
#error "Must give -DEDGE=x option to gcc (e.g. -DEDGE=9 for 9x9 go)"
#endif  /* ifndef EDGE */


#define BOARD_SIZE (EDGE*EDGE)
#define SIZE (BOARD_SIZE + 1)
#define ON_BOARD(x)      ((x) > 0 && (x) <= BOARD_SIZE)
#define OVER_BOARD(i)  for (int i = BOARD_SIZE; i > 0; i--)
#define OVER_BOARD_AND_NULL(i)  for (int i = BOARD_SIZE; i >= 0; i--)
#define STRING_REP_SIZE  (10 * BOARD_SIZE + 1000)
#define POLL_FREQUENCY 17


int Neighbors[SIZE][8]= {{0}};
int HashContribs[SIZE][4] = {{0}};
int LowestBit[256];

#include "types.c"
#include "board.c"

#if (!defined(HASHMEM)) || HASHMEM <= 0
#undef HASHMEM
#define HASHMEM 0
#define BADHASHSIZE 1024
#else
#define BADHASHSIZE ((1 << 20) * HASHMEM / sizeof(HashEntry))
#endif /* HASHMEM */

#define YY(x) (BADHASHSIZE >> x)
#define HASHSIZE ((YY(1) | YY(2) | YY(3) | YY(4) | YY(5) | YY(6) | YY(7) \
                | YY(8) | YY(9) | YY(10) | YY(11) | YY(12) | YY(13) \
                | YY(14) | YY(15) | YY(16) | YY(17) | YY(18) | YY(19) \
                | YY(20) | YY(21) | YY(22) | YY(23) | YY(24) | YY(25) \
                | YY(26) | YY(27) | YY(28) | YY(29) | YY(30) | YY(31)) + 1)



HashEntry TransTable[HASHSIZE];

struct {
    int side;
    int point;
} History[SIZE * 10] = {{0, 0}};



int distance(int x, int y){
    x--; y--;
    int a = abs(x % EDGE - y % EDGE);
    int b = abs(x / EDGE - y / EDGE);
    return a + b;
}

int are_adjacent(int x, int y){
    return distance(x, y) == 1;
}

void initNeighborMatrix(){
    if (EDGE < 2 || EDGE > 19){
        printf("This binary was compiled for %dx%d boards, "
               "but only sizes 2x2 to 19x19 are permitted.  \n"
               "Weird, huh?  Exiting...", EDGE, EDGE);  exit(2);
    }

    int deltas[4] = {1, -1, EDGE, -EDGE};
    OVER_BOARD(pt){
        int j = 0;
        for (int d = 0; d < 4; d++){
            int delta = deltas[d];
            if (ON_BOARD(pt + delta) && are_adjacent(pt, pt + delta))
                Neighbors[pt][j++] = pt + delta;
        }
        for (; j < 8; j++)
            Neighbors[pt][j++] = 0;
    }
}

void initHashContribs(){
    int i, j;
    OVER_BOARD(pt){
        int a = rand(); int b = rand();
        int contribs[3] = {0, a, b};
        for (i = 0; i < 3; i++){
            for (j = 0; j < 3; j++){
                HashContribs[pt][i^j] = contribs[i] ^ contribs[j];
            }
        }
    }
}

void initTransTable(){
    memset(TransTable, -1, sizeof(TransTable));
    for (int i = 0; i < HASHSIZE; i++)
        TransTable[i].point = EDGE + 2;
}

void initLowestBits()
{
    LowestBit[0] = 0;
    for (int i = 1; i < 256; i++){
        LowestBit[i] = (i % 2 == 1 ? 0 : 1 + LowestBit[i / 2]);
    }
}

int getLowestBit(unsigned int n){
#ifdef __GNUC__
    return __builtin_ctz(n);
#endif

    if (n == 0) return 0;
    int lowend = n & 255;
    if (lowend)
        return LowestBit[lowend];
    else
        return 8 + getLowestBit(n >> 8);
}



int wasSeen[SIZE] = {1, 0};
int allSeen[SIZE];
int * allSeenEnd = allSeen;
int libValue;


int findLiberty(const Position * const pos, const int where)
{
    if (wasSeen[where])
        return 0;

    const int stone = getStone(pos, where);
    if (stone != libValue)
        return !stone;
    else {
        *allSeenEnd++ = wasSeen[where] = where;

        const int * const nei = &Neighbors[where][0];
        return findLiberty(pos, nei[0]) || findLiberty(pos, nei[1]) ||
               findLiberty(pos, nei[2]) || findLiberty(pos, nei[3]);
    }
}

int isGroupAlive(const Position * const pos, const int where)
{
    libValue = getStone(pos, where);
    if (libValue == 0) return 1;
    int result = findLiberty(pos, where);
    while (allSeenEnd > allSeen){
        wasSeen[*--allSeenEnd] = 0;
    }
    return result;
}


void deleteGroup(Position * const pos, const int where, const int value){
    int stoneThere = getStone(pos, where);
    if (stoneThere != value)
        return;

    setStone(pos, where, 0, stoneThere);
    for (const int * nei = &Neighbors[where][0]; *nei; nei++)
        deleteGroup(pos, *nei, value);
}

void passTurn(Position * const pos)
{
    pos->turn ^= 3;
    pos->hash = ~pos->hash;
}

void placeStone(Position * const pos, const int where){
    const int who = pos->turn ^ 3;
    setStone(pos, where, pos->turn, 0);
    pos->lastMoves[0] = pos->lastMoves[1];
    pos->lastMoves[1] = where;

    for (const int * nei = &Neighbors[where][0]; *nei; nei++){
        if (!isGroupAlive(pos, *nei))
            deleteGroup(pos, *nei, who);
    }
    if (!isGroupAlive(pos, where))
        deleteGroup(pos, where, who ^ 3);

    passTurn(pos);
}



int  getTurn(Position * pos){ return pos->turn; }
void setTurn(Position * pos, int turn){ pos->turn = turn; }



int tallyPos(const Position * const pos)
{
    return pos->counts[pos->turn] * 2 - pos->counts[1] - pos->counts[2];
}


#include "shell.c"
#include "search.c"


char NAMES[] = "?BW";
void printMoveHistory()
{
    for (int i = 0; History[i].side; i++){
        if (i) printf("; ");
        printf("%d. %c at %d", i+1, NAMES[History[i].side], History[i].point);
    }
    printf("\n");
}

int getDepthFromCmdLine(int argc, char * argv[]){
    if (argc <= 1){
        return -1;
    } else {
        return atoi(argv[1]);
    }
}

void printHashFreqs(){
    int freqs[SIZE] = {0};
    for (int i = 0; i < HASHSIZE; i++)
        freqs[TransTable[i].point]++;
    for (int i = 0; i <= BOARD_SIZE; i++)
        printf("%d: %d; ", i, freqs[i]);
    printf("\n");
}

void initialize(){
    setbuf(stdout, NULL);  setbuf(stdin, NULL);
    initNeighborMatrix();
    initHashContribs();
    initTransTable();
    initLowestBits();
    initPointNames();
}

int main(int argc, char * argv[])
{
    showHelp(NULL);

    int maxDepth = 1 + getDepthFromCmdLine(argc, argv);
    int depth;

    initialize();
    printf("A9 is %d\n", pointFromName("A9"));
    printf("A8 is %d\n", pointFromName("a8"));
    Position pos;
    emptyPosition(&pos);

    printf("%s\n\n", drawPosition(&pos));

    while (1){
        for (depth = 1; depth != maxDepth; depth++){
            int point = 0;

            abInfo info = newABInfo();
            info.pos = pos;
            int value = alphaBeta(&pos, depth, &point, -1000000, 1000000, &info);
            double endingTime = getPreciseTime();
            long long int timeTaken = endingTime - info.startTime + 1;

            pos = info.pos;

            if (info.aborted){
                printf("# Aborted!\n");
                depth = 0; break;
            }

            char pointname[7] = "      ";
            strncpy(pointname, &POINT_NAMES[6 * point], 6);
            printf("# Depth %d: move %s  (took %lld sec, %lld nodes; value %d)\n",
                depth, pointname, timeTaken, info.nodes, value);
            // printHashFreqs();
        }
        if (depth)
            break;
    }

    quit(NULL); return 0;
}

// types.c
// Copyright 2008 Tom Plick.
// Begorra is released under GPL v2: see file COPYING for info.

#ifndef LINE_C
#define LINE_C

typedef unsigned short BoardInt;
#define BFWIDTH  (8 * sizeof(BoardInt))
#define BFLENGTH (SIZE / BFWIDTH + 1)

typedef struct {
    char stones[SIZE];
    BoardInt empty[BFLENGTH];
    char turn;
    int lastMoves[2];
    int counts[3];
    unsigned int hash;
} Position;

typedef struct {
    char depth;
    char point;
} HashEntry;

typedef struct {
    unsigned long long int nodes;
    int aborted;
    double startTime;
    int commandNumber;
    Position pos;
} abInfo;

#endif  /* LINE_C */


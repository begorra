#!/usr/bin/perl
use Time::HiRes qw(gettimeofday);

my @SIZES = (0, 1, 4, 16, 64);

sub time_execution {
    my $cmdline = shift;
    my $start = gettimeofday();
    my $result = system($cmdline);
    my $end = gettimeofday();

    if ($result != 0){
        print "\n\n$cmdline: Abnormal termination; quitting.\n";
        exit(1);
    }

    return int($end - $start) + 1;
}

system("date");
print "Depth";
print "\t${_}M" for (@SIZES);
print "\n";

foreach my $depth (5..1000){
    print "$depth";
    foreach my $size (@SIZES){
        my $runtime = time_execution("./exe$size $depth 2>&1 > /dev/null");
        print "\t$runtime";
    }
    print "\n";
}
system("date");
